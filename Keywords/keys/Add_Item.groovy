package keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

public class Add_Item {
	/**
	 * Keyword for specific swipe action
	 * Swipe to default element: Walter Henley item
	 */
	@Keyword
	def static swipeHenley(){
		def exist
		for (int i = 0; 0 < 1; i++){
			exist = Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - henley'), 2)
			if(exist){
				break
			}else {
				keys.Utils.swipe("left")
				Mobile.delay(1)
			}
		}
	}

	/**
	 * Keyword for specific swipe action
	 * Swipe to default element: Shrug bag item
	 */
	@Keyword
	def static swipeShrug(){
		def exist
		for (int i = 0; 0 < 1; i++){
			exist = Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - shrug_bag'), 2)
			if(exist){
				break
			}else {
				keys.Utils.swipe("right")
				Mobile.delay(1)
			}
		}
	}

	/**
	 * Keyword for specific click action
	 * Click to default element: Walter Henley item
	 */
	@Keyword
	def static clickHenley(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - henley'), 2)
		Mobile.tap(findTestObject('Object Repository/ShrineGallery/android.widget.Button - henley'), 1)
	}

	/**
	 * Keyword for specific click action
	 * Click to default element: Shrug bag item
	 */
	@Keyword
	def static clickShrug(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - shrug_bag'), 2)
		Mobile.tap(findTestObject('Object Repository/ShrineGallery/android.widget.Button - shrug_bag'), 1)
	}

	/**
	 * Keyword for specific click action
	 * Click to default element: Cart button with items
	 */
	@Keyword
	def static clickCart(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - cart_with_item'), 5)
		Mobile.tap(findTestObject('Object Repository/ShrineGallery/android.widget.Button - cart_with_item'), 1)
	}

	/**
	 * Keyword for specific click action
	 * Click to default element: Cart button with 1 items
	 */
	@Keyword
	def static clickCart_1(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - cart_1_item'), 5)
		Mobile.tap(findTestObject('Object Repository/ShrineGallery/android.widget.Button - cart_1_item'), 1)
	}

	/**
	 * Keyword for specific click action
	 * Click to default element: Cart button with 0 items
	 */
	@Keyword
	def static clickCart_0(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - cart_0'), 5)
		Mobile.tap(findTestObject('Object Repository/ShrineGallery/android.widget.Button - cart_0'), 1)
	}
	
	/**
	 * Keyword for specific click action
	 * Click to default element: Arrow button
	 */
	@Keyword
	def static clickDownArrow(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - arrow_down'), 5)
		Mobile.tap(findTestObject('Object Repository/ShrineGallery/android.widget.Button - arrow_down'), 1)
	}

	/**
	 * Keyword for specific click action
	 * Click to default element: Clear Cart
	 */
	@Keyword
	def static clearCart(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - clear_cart'), 5)
		Mobile.tapAndHold(findTestObject('Object Repository/ShrineGallery/android.widget.Button - clear_cart'), 0, 0)
	}

	/**
	 * Keyword for specific cancel click action in Cart list
	 * Click to default element: Walter Henley item
	 */
	@Keyword
	def static cancelHenley(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ShrineGallery/android.widget.Button - minus (1)'), 5)
		Mobile.tap(findTestObject('Object Repository/ShrineGallery/android.widget.Button - minus (1)'), 1)
	}

	/**
	 * Check total price when cancel 1 item
	 * @param value is main value to be compare with
	 * @param compare is comparison value for main value
	 * @return main value in String
	 */
	@Keyword
	def static checkTotal(String value, String compare){
		String[] values = value.split( '\n' )
		String list_val = values[1]
		String number = list_val.replaceAll("[^a-zA-Z0-9.]+","")
		float num = number as float
		float num2 = compare as float
		if(num2 == 0){
			if(num > num2){
				KeywordUtil.markPassed("Comparing value Passed")
			}else KeywordUtil.markFailed("Comparing value Failed")
		}else{
			if(num < num2){
				KeywordUtil.markPassed("Comparing value Passed")
			}else KeywordUtil.markFailed("Comparing value Failed")
		}
		return number
	}

	/**
	 * Check total price when no item in cart
	 * @param value is main value to be compare with
	 * @param compare is comparison value for main value
	 * @return main value in String
	 */
	@Keyword
	def static checkTotal_0(String value){
		String[] values = value.split( '\n' )
		String list_val = values[1]
		String number = list_val.replaceAll("[^a-zA-Z0-9.]+","")
		float num = number as float
		if(num == 0){
			KeywordUtil.markPassed("Comparing value Passed")
		}else KeywordUtil.markFailed("Comparing value Failed")
		return number
	}
	
	/**
	 * Check total price when add new item
	 * @param value is main value to be compare with
	 * @param compare is comparison value for main value
	 * @return main value in String
	 */
	@Keyword
	def static checkTotal_(String value, String compare){
		String[] values = value.split( '\n' )
		String list_val = values[1]
		String number = list_val.replaceAll("[^a-zA-Z0-9.]+","")
		float num = number as float
		float num2 = compare as float
		if(num > num2){
			KeywordUtil.markPassed("Comparing value Passed")
		}else KeywordUtil.markFailed("Comparing value Failed")
		return number
	}
}
