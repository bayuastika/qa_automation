package keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class Login {

	/**
	 * Check landing page after launch the application 
	 */
	def static verifyLandingPage(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ImageSlider/android.view.View - 1st'), 5)
	}

	/**
	 * Check Shrine Gallery Image slider in landing page
	 */
	def static verifyShrineImage(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/ImageSlider/android.view.View - 2nd'), 5)
	}

	/**
	 * Click Shrine Gallery Image slider in landing page
	 */
	def static clickShrineImage(){
		Mobile.tapAndHold(findTestObject('Object Repository/ImageSlider/android.view.View - 2nd'), 0, 0)
		Mobile.delay(2)
	}

	/**
	 * Check Shrine login page in the application
	 * Assertion with username field
	 */
	def static verifyLoginShrine(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/LoginPage/android.widget.EditText - Username'), 5)
	}

	/**
	 * Check and Input username field in Shrine Login Page
	 * Value input only by default 
	 */
	def static inputUsername(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/LoginPage/android.widget.EditText - Username'), 5)
		Mobile.tap(findTestObject('Object Repository/LoginPage/android.widget.EditText - Username'), 0)
		Mobile.delay(2)
		keys.Utils.inputText("user")
	}

	/**
	 * Check and Input password field in Shrine Login Page
	 * Value input only by default
	 */
	def static inputPassword(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/LoginPage/android.widget.EditText - Password'), 5)
		Mobile.tap(findTestObject('Object Repository/LoginPage/android.widget.EditText - Password'), 0)
		Mobile.delay(2)
		keys.Utils.inputText("user")
	}

	/**
	 * Check and click next button in Shrine Login Page
	 */
	def static clickNextBtn(){
		Mobile.waitForElementPresent(findTestObject('Object Repository/LoginPage/android.widget.Button - next'), 5)
		Mobile.tap(findTestObject('Object Repository/LoginPage/android.widget.Button - next'), 0)
	}

	/**
	 * Keyword for open Shrine Gallery
	 * Executes all function that relates to open Shrine Gallery
	 */
	@Keyword
	def static openShrineGallery(){
		verifyLandingPage()
		keys.Utils.swipe("left")
		verifyShrineImage()
		clickShrineImage()
		verifyLoginShrine()
		inputUsername()
		inputPassword()
		clickNextBtn()
		Mobile.delay(2)
	}
}
