package keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import org.openqa.selenium.interactions.Actions

public class Utils {
	/**
	 * Keyword for swipe up, down, left or right
	 * @param orientation value only with up, down, left and right only
	 * @return
	 */
	@Keyword
	def static swipe(String orientation){
		switch (orientation.toLowerCase()){
			case "up":
				Mobile.swipe(0, swipeValue("start", "vertical"), 0, swipeValue("end", "vertical"))
				break
			case "down":
				Mobile.swipe(0, swipeValue("end", "vertical"), 0, swipeValue("start", "vertical"))
				break
			case "left":
				Mobile.swipe(swipeValue("start", "horizontal"), swipeValue("end", "vertical"), swipeValue("end", "horizontal"), swipeValue("end", "vertical"))
				break
			case "right":
				Mobile.swipe(swipeValue("end", "horizontal"), swipeValue("end", "vertical"), swipeValue("start", "horizontal"), swipeValue("end", "vertical"))
				break
		}
	}
	/**
	 * Keyword to fill param in keyword Swipe
	 * @param dimen is for position for swipe action. value only 'start' & 'end'
	 * @param orientation is for the orientation to do swipe action. value only 'vertical' & 'horizontal'
	 * @return integer value that to use in swipe function
	 */
	@Keyword
	def static int swipeValue(String dimen, String orientation) {
		int value = 0
		dimen = dimen.toLowerCase()
		orientation = orientation.toLowerCase()
		int device_Height = Mobile.getDeviceHeight()
		int device_Width = Mobile.getDeviceWidth()
		switch (orientation){
			case "vertical":
				switch (dimen){
					case "start":
					value = device_Height * 0.7
					break
					case "end":
					value = device_Height * 0.3
					break
				}
				break
			case "horizontal":
				switch (dimen){
					case "start":
					value = device_Width * 0.6
					break
					case "end":
					value = device_Width * 0.2
					break
				}
				break
		}
		return value
	}

	/**
	 * Function to get apk path from properties file
	 * @return apk path in String
	 */
	def static path(){
		Properties properties = new Properties()
		File propertiesFile = new File('sampleData.properties')
		propertiesFile.withInputStream { properties.load(it) }
		return properties.apk_path
	}

	/**
	 * Input character through the device keypad
	 * @param text is character or word in String to be input
	 */
	def static inputText(String text) {
		def act = new Actions(MobileDriverFactory.getDriver())
		act.sendKeys(text).perform()
		Mobile.pressBack()
	}

	/**
	 * Keyword to get attribute of the element
	 * @param obj is TestObject or the Element
	 * @param attribute is attribute that exist in the element, you could see the attribute when spy the element
	 * @return value in String
	 */
	@Keyword
	def static getAttribute(TestObject obj, String attribute){
		String value = Mobile.getAttribute(obj, attribute, 5)
		return value
	}
}
