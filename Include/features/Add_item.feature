Feature: Adding item into cart in Shrine Gallery

  Scenario: Check total price after cancel item in cart
    Given In Shrine gallery homepage
    When Swipe left the items until see Walter henley shirt
    And Click it
    And Swipe right the items until see Shrug bag
    And Click it again
    When Click cart button
    Then Total price shown
    When Cancel Walter henley item
    Then Total price changed
