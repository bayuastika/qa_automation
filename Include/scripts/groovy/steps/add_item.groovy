package steps
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class add_item {
	/**
	 * The step definitions below
	 */
	String compare = ""
	@Given("In Shrine gallery homepage")
	def In_Shrine_gallery_homepage() {
		keys.Login.openShrineGallery()
	}

	@When("Swipe left the items until see Walter henley shirt")
	def Swipe_left_the_items_until_see_Walter_henley_shirt() {
		keys.Add_Item.swipeHenley()
	}
	
	@And("Click it")
	def Click_it(){
		keys.Add_Item.clickHenley()
	}
	
	@And("Swipe right the items until see Shrug bag")
	def Swipe_right_the_items_until_see_Shrug_bag(){
		keys.Add_Item.swipeShrug()
	}
	
	@And("Click it again")
	def Click_it_again(){
		keys.Add_Item.clickShrug()
	}
	
	@When("Click cart button")
	def Click_cart_button() {
		keys.Add_Item.clickCart()
	}

	@Then("Total price shown")
	def Total_price_shown() {
		String data = keys.Utils.getAttribute(findTestObject("Object Repository/ShrineGallery/android.view.View - total"), "Content-desc")
		compare = keys.Add_Item.checkTotal(data, "0")
	}
	
	@When("Cancel Walter henley item")
	def Cancel_Walter_henley_item() {
		keys.Add_Item.cancelHenley()
	}

	@Then("Total price changed")
	def Total_price_changed() {
		String data = keys.Utils.getAttribute(findTestObject("Object Repository/ShrineGallery/android.view.View - total2"), "Content-desc")
		keys.Add_Item.checkTotal(data, compare)
	}
}