# README #


### Automation with Katalon Tools ###

* In this repository contains two type of test cases:
	1. Created with custom keyword.
	2. Created with cucumber.
* And also contain one test suite for run all test cases.

### Environment ###
* Device:
	* Emulator Pixel 3
	* Android 9
	* Screen 5.46"
* Apk can be downloaded [here.](https://github.com/flutter/gallery/releases)

### How do I get set up? ###

* If you don't have Katalon Studio, please install it first [here.](https://www.katalon.com/)
* After install the Katalon Studio, please download plugin for Katalon report [here.](https://store.katalon.com/product/59/Basic-Report)
  And you can install the plugin through the Katalon Studio itself.
* Clone this repository after finish install Katalon Studio.
* Before you run the Automation please change your .apk path in file 'sampleData.properties' and follow the instruction inside that file.
* How to run tests:
	1. Launch your emulator
	2. Go to Katalon Studio with code from the repository
	3. Change your .apk path in 'sampleData.properties' file.
	4. Open 'Test Suites' folder in left panel Katalon Studio and click TestSuite file.
	5. Execute TestSuite with click arrow down near green play button and choose Android if your emulator is using android.
	   For more information you can read [here.](https://docs.katalon.com/katalon-studio/docs/test-suite.html#execute-a-test-suite)

### Get a Report ###

* Report will generate automatically after finish run Test Suites.
* You can see the report in Reports folder in Katalon Project.
* If report doesn't generate after finish run Test Suites, please check your plugin for report is already installed or not.
