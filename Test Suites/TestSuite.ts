<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>49824f87-128b-437a-85a5-2c44a81ed838</testSuiteGuid>
   <testCaseLink>
      <guid>f40a72f2-f4e3-4b64-88d9-d1c18ae96512</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddItem</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>535853fa-41fb-4416-872b-ffb067a9886c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClearCart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31854e2f-d7fa-48fa-8ef2-56e041ec6a06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CancelItem</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
