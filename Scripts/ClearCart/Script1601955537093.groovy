import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'Opening Shrine Gallery'
CustomKeywords.'keys.Login.openShrineGallery'()

'Swipe left the item until get Walter henley shirt'
CustomKeywords.'keys.Add_Item.swipeHenley'()

'Click Walter henley item '
CustomKeywords.'keys.Add_Item.clickHenley'()

'Swipe right the item until get Shrug bag'
CustomKeywords.'keys.Add_Item.swipeShrug'()

'Click Shrug bag item'
CustomKeywords.'keys.Add_Item.clickShrug'()

'Click cart button in bottom right corner'
CustomKeywords.'keys.Add_Item.clickCart'()

data = CustomKeywords.'keys.Utils.getAttribute'(findTestObject('ShrineGallery/android.view.View - total'), 'Content-desc')

'Check the total price from the items in cart'
total1 = CustomKeywords.'keys.Add_Item.checkTotal'(data, '0')

'Click clear cart button'
CustomKeywords.'keys.Add_Item.clearCart'()

'Click cart button again'
CustomKeywords.'keys.Add_Item.clickCart_0'()

data0 = CustomKeywords.'keys.Utils.getAttribute'(findTestObject('ShrineGallery/android.view.View - total_0'), 'Content-desc')

'Check total price is become zero or not'
CustomKeywords.'keys.Add_Item.checkTotal_0'(data0)

